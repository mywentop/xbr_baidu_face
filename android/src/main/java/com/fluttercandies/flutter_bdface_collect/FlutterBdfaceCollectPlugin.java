package com.fluttercandies.flutter_bdface_collect;

import android.app.Activity;
import androidx.annotation.NonNull;
import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;

/**
 * FlutterBdfaceCollectPlugin
 */
public class FlutterBdfaceCollectPlugin implements FlutterPlugin, MethodCallHandler, ActivityAware {
    private static final String channelName = "com.fluttercandies.bdface_collect";

    private MethodChannel channel;
    private Activity activity;
    private FlutterBdFaceSetting flutterBdFaceSetting;

    public FlutterBdFaceSetting getFlutterBdFaceSetting() {
        if (flutterBdFaceSetting==null){
            flutterBdFaceSetting = FlutterBdFaceSetting.getInstance();
        }
        return flutterBdFaceSetting;
    }

    @Override
    public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
        channel = new MethodChannel(flutterPluginBinding.getBinaryMessenger(), channelName);
        channel.setMethodCallHandler(this);
    }

    @Override
    public void onMethodCall(@NonNull MethodCall call, @NonNull final Result result) {
        switch (call.method) {
            case MethodConstants.GetPlatformVersion:
                result.success("Android " + android.os.Build.VERSION.RELEASE);
                break;
            case MethodConstants.Init:
                getFlutterBdFaceSetting().init(activity,(String) call.arguments,result);
                break;
            case MethodConstants.Collect:
                getFlutterBdFaceSetting().collect(activity, call.arguments,result);
                break;
            case MethodConstants.UnInit:
                getFlutterBdFaceSetting().unInit(result);
                break;
            default:
                result.notImplemented();
        }
    }

    @Override
    public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
        channel.setMethodCallHandler(null);
        getFlutterBdFaceSetting().dispose();
    }

    @Override
    public void onAttachedToActivity(@NonNull ActivityPluginBinding binding) {
        activity = binding.getActivity();
        getFlutterBdFaceSetting().collectBack(binding);
    }

    @Override
    public void onDetachedFromActivityForConfigChanges() {
        activity = null;
        getFlutterBdFaceSetting().dispose();
    }

    @Override
    public void onReattachedToActivityForConfigChanges(@NonNull ActivityPluginBinding binding) {
        activity = binding.getActivity();
        getFlutterBdFaceSetting().dispose();
    }

    @Override
    public void onDetachedFromActivity() {
        activity = null;
        getFlutterBdFaceSetting().dispose();
    }
}
