# xbr_baidu_face

#### 介绍
XBR 百度人脸采集封装插件

#### 使用说明

1. 前往百度智能云官网申请license文件（需配置APP签名，.jks生成和signing自行百度）
2. Android：在android-main下面新建assets文件夹，放入 idl-license.face-android 文件
    IOS：在ios-Runner下面放入 idl-license.face-ios 文件
3. 初始化百度FACE后即可开始采集

#### 更新说明
1. 安卓端FACE-SDK升级到：4.1.5，在 FlutterBdfaceCollect 基础上完成 Android部分机型的修复,如小米9-pro等
2. 基于SDK新特性，重写了大量Android端代码

#### 使用
~~~Dart
    // 初始化：
    void initBdFace() {
        if (Platform.isAndroid){
          FlutterBdfaceCollect.instance.init(androidLicenseId);
        }else{
          FlutterBdfaceCollect.instance.init(iosLicenseId);
        }
    }
    //开始采集：
    Future<void> liveDetect() async {
        FaceConfig config = FaceConfig(livenessTypes: Set.from(LivenessType.all.sublist(1, 4)));
        CollectResult res = await FlutterBdfaceCollect.instance.collect(config);
        if(res.error.isNotEmpty){
          print(res.error);
          return;
        }
        print(res.imageSrcBase64);
    }
~~~

#### 注意事项

1. 采集前需要配置相机权限，并使用动态权限
2. 如果日志中出现异常：java.io.FileNotFoundException: idl-license.face-android，说明没有license文件，或license文件放错位置


#### 参与贡献

1.  XBR团队
