import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'dart:async';

import 'package:xbr_baidu_face/flutter_bdface_collect.dart';
import 'package:xbr_baidu_face/model.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
    initBdFace();
  }

  ///初始化百度FACE
  void initBdFace() {
    if (Platform.isAndroid){
       FlutterBdfaceCollect.instance.init("driver-face-face-android");
    }else{
       FlutterBdfaceCollect.instance.init("iosLicenseId");
    }
    // Config config = Config(livenessTypeList: [LivenessTypes.random()]);
    // if (Platform.isAndroid) {
    //   BaiduFace.instance.init('driver-face-face-android', config: config);
    // } else if (Platform.isIOS) {
    //   BaiduFace.instance.init('XiaobiaorenApp-face-ios', config: config);
    // }
  }

  ///開始采集
  Future<void> liveDetect() async {
    FaceConfig config = FaceConfig(livenessTypes: Set.from(LivenessType.all.sublist(1, 4)));
    CollectResult res = await FlutterBdfaceCollect.instance.collect(config);
    debugPrint('采集错误结果:${res.error.isNotEmpty} 内容：${res.error}');
    debugPrint('采集原图imageCropBase64:${res.imageSrcBase64.isNotEmpty}');
    debugPrint('采集抠图imageSrcBase64:${res.imageCropBase64.isNotEmpty}');
    if(res.error.isNotEmpty){
      return;
    }
    setState(() {
      _bytes = base64Decode(res.imageCropBase64);
    });
  }
  Uint8List? _bytes;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title:  const Text("百度人脸采集调试"),
        ),
        body: Container(
          padding: const EdgeInsets.all(30),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text('欢迎使用小镖人百度人脸采集系统', style: style1),
              const SizedBox(height: 16),
              const Text('1：创建JKS秘钥文件，完成APP签名,自行百度查看方法,前往百度智能云官网申请license文件', style: style2),
              const SizedBox(height: 10),
              const Text('2-1：在android-main下面新建assets文件夹，放入 idl-license.face-android 文件 ', style: style2),
              const SizedBox(height: 10),
              const Text('2-2：在ios-Runner下面放入 idl-license.face-ios 文件 ', style: style2),
              const SizedBox(height: 10),
              const Text('3：初始化百度FACE,点击第4步即可开始采集', style: style2),
              const SizedBox(height: 10),
              OutlinedButton(
                onPressed: () {liveDetect();},
                child: const Text("4：点击开始采集", style: style1),
              ),
              if(_bytes!=null)
              SizedBox(
                width: 150,
                height: 150,
                child: Image.memory(_bytes!,fit:BoxFit.cover),
              ),
              const Text('注意：example 未配置动态权限，测试时需要手动赋予权限', style: style2),
              const Text("如果日志中出现异常：java.io.FileNotFoundException: idl-license.face-android，说明没有license文件，或license文件放错位置")
            ],
          ),
        ),
      ),
    );
  }

}

const TextStyle style1 = TextStyle(fontWeight: FontWeight.bold, fontSize: 24);
const TextStyle style2 = TextStyle(fontWeight: FontWeight.bold, fontSize: 16);
