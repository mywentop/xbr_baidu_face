//
//  Generated file. Do not edit.
//

// clang-format off

#import "GeneratedPluginRegistrant.h"

#if __has_include(<xbr_baidu_face/FlutterBdfaceCollectPlugin.h>)
#import <xbr_baidu_face/FlutterBdfaceCollectPlugin.h>
#else
@import xbr_baidu_face;
#endif

@implementation GeneratedPluginRegistrant

+ (void)registerWithRegistry:(NSObject<FlutterPluginRegistry>*)registry {
  [FlutterBdfaceCollectPlugin registerWithRegistrar:[registry registrarForPlugin:@"FlutterBdfaceCollectPlugin"]];
}

@end
